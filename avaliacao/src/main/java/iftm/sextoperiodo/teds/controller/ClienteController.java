package iftm.sextoperiodo.teds.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import iftm.sextoperiodo.teds.been.Cliente;
import iftm.sextoperiodo.teds.file.GerenciamentoArquivo;

public class ClienteController {

	//o ideal � buscar este arquivo nas propriedades do sistema
	private static String file = "/home/douglas/teste.txt";

	private GerenciamentoArquivo ga;

	public ClienteController() {		
		ga = new GerenciamentoArquivo(file);
	}

	public void registraCliente(String json) throws Exception {

		Gson gson = new Gson();

		Cliente c = (Cliente) gson.fromJson(json, Cliente.class);

		if (c.getCpf() == null || (c.getCpf() != null && (c.getCpf().trim().isEmpty() || c.getCpf().length() < 11))) {
			throw new Exception("CPF vazio!");

		} else if (c.getCpf() != null) {
			try {
				ga.writeFile(json);
				
			} catch (IOException e) {
				e.printStackTrace();
				throw new Exception ("Ocorreu um erro ao registrar o cliente");
			}
		}
	}

	public List<Cliente> retornaClientes() throws Exception{

		try {
			List<String> data = ga.readFile();
			List<Cliente> clientes = new ArrayList<Cliente>();
			Gson gson = new Gson();
			

			if (data != null) {
				for (String s : data) {
					clientes.add(gson.fromJson(s, Cliente.class));
				}
			}

			return clientes;

		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Ocorreu um erro ao buscar os clientes.");
		}
	}

}
